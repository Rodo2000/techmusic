console.log("ready");

// Constantes del formulario
let usuarios = [];
const btnRegistro = document.querySelector(".btn-registro");
const contenidoRegistro = document.querySelector(".registro");
const form = document.querySelector("#formRegistro");
const user = document.querySelector(".user");
const pw = document.querySelector(".pw");
const close = document.querySelector(".close");
const bienvenida = document.querySelector(".contenido-hero");
bienvenida.style.display = "none";
const techMusic = document.querySelector(".techMusic");
techMusic.style.display = "none";
const logo = document.querySelector(".logo");
logo.style.display = "none";
// loading
window.onload = function () {
  var contenedor = document.getElementById("contenedor_carga");
  contenedor.style.visibility = "hidden";
  contenedor.style.opacity = "0";
};

// abre formulario
btnRegistro.addEventListener("click", () => {
  console.log("diste click");
  contenidoRegistro.style.display = "block";
});

// cierra formulario
close.addEventListener("click", () => {
  console.log("diste click");
  contenidoRegistro.style.display = "none";
});

form.addEventListener("submit", (e) => {
  e.preventDefault();
  registrarUsuario();
});

// registro de user
const registrarUsuario = () => {
  console.log("registrar...");
  console.log(user.value);
  console.log(pw.value);
  let usuario = {
    user: user.value,
    pw: pw.value,
  };
  usuarios = [...usuarios, usuario];
  localStorage.setItem("usuarios", JSON.stringify(usuarios));
  console.log(usuarios);
  form.reset();
};

setTimeout(() => {
  logo.style.display = "block";
}, 1000);

setTimeout(() => {
  bienvenida.style.display = "block";
}, 2000);
setTimeout(() => {
  techMusic.style.display = "block";
}, 2400);
